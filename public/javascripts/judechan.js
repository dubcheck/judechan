$(function() {
	var PostsView = Backbone.View.extend({
		initialize: function(options) {
			console.log('PostsView.initialize');
			this.listenTo(options.model, 'remove', this.remove);
			this.listenTo(Posts, 'reset', this.remove);
			this.listenTo(App, 'change:thread', this.remove);
		},
		template: Handlebars.compile('\
				<p id="post_{{_id}}">\
				<span id="author">{{author}}</span>&nbsp;\
				<span id="subject">{{subject}}</span>&nbsp;\
				<span id="date">{{date}}</span>&nbsp;\
				<span id="post_id">{{_id}}</span><br>\
				<span id="content">{{content}}</span><br>\
				{{#if media}}\
					<span class="media_orig_name" id="media_orig_name_{{media_orig}}">{{media_orig}}</span>\
					<img class="postMedia" id="thumb_post_{{_id}}" src="resources/wait.gif" data-src="{{media}}"></img>\
				{{/if}}\
				</p>\
			'),
		render:function(attributes) {
			let self = this;
			console.log('PostsView.render');
			let content = this.template(attributes);
			$('#content').append(content);
			$('img#thumb_post_' + this.model.id).click(function(e) {
				console.log(self.model.get('type'));
				if (self.model.get('type') === 'image') {
					e.preventDefault();
					if(/_thumb/.exec($(this).attr('src'))){
						$(this).attr('src', $(this).attr('src').replace(/_thumb/, ''));
					} else {
						$(this).attr('src', $(this).attr('src').replace(/([.].*)/, '_thumb$1'));
					}
				} else if (self.model.get('type') === 'video') {
					if(/_thumb/.exec($(this).attr('src'))){
						let ext = /(\..*)/.exec(self.model.get('media_orig'))[1];
						$(this).parent().append('<video autoplay loop controls src=' + self.model.get('media').replace(/_thumb.*$/, '') + ext +
							' type="video/' + ext.replace(/./, '') + '"></video>');
						//$(this).attr('src', $(this).attr('src').replace(/_thumb/, ''));
					}
				}
			});
			if (this.model.get("media"))
				$('#post_' + this.model.id + ' img').unveil(1488);
			this.el = '#post_' + this.model.id;
			this.$el = $(this.el);
		},
		remove: function() {
			console.log('PostsView.remove');
			console.log(this.$el);
			this.$el.remove();
		}
	});

	var ThreadsView = Backbone.View.extend({
		initialize: function(options) {
			console.log('ThreadsView.initialize');
			this.listenTo(options.model, 'remove', this.remove);
			this.listenTo(Threads, 'reset', this.remove);
			this.listenTo(App, 'change:board', this.remove);
		},
		template: Handlebars.compile('\
			<div id="thread_{{_id}}">\
			<a href="#boards/{{board}}/threads/{{_id}}/posts" id="showThread_{{_id}}">{{_id}}</a>\
			<input id="thread_{{_id}}_remove" type="checkbox">\
				{{#each posts}}\
					<p id="post_{{_id}}">\
						<span id="author">{{author}}</span>&nbsp;\
						<span id="subject">{{subject}}</span>&nbsp;\
						<span id="date">{{date}}</span>&nbsp;\
						<span id="post_id">{{_id}}</span><br>\
						<span id="content">{{content}}</span><br>\
						{{#if media}}\
							<span class="media_orig_name" id="media_orig_name_{{media_orig}}">{{media_orig}}</span>\
							<img src={{media}}></img>\
						{{/if}}\
					</p>\
				{{/each}}\
			</div>'),
		render: function(attributes) {
			let self = this;
			console.log('ThreadsView.render');
			console.log(attributes);
			let content = this.template(attributes);
			$('#content').append(content);
			this.el = '#thread_' + this.model.id;
			this.$el = $(this.el);
		},
		remove: function() {
			console.log('ThreadsView.remove');
			console.log(this.$el);
			this.$el.remove();
		},
	});

	var AppView = Backbone.View.extend({
		el:'#formContent',
		templateThread: Handlebars.compile('<span>Autor: <input type="text" id="newThreadAuthor" value="Janonymous"></span><br>\
					<span>Predmet: <input type="text" name="subject" id="newThreadSubject"></span><br>\
					<span>Komentár: <input type="text" name="content" id="newThreadContent"></span><br>\
					<span>Súbor: <input type="file" name="img" id="newThreadFile"></span><br>\
					<span>Adresa: <input type="text" name="link" id="newThreadLink"></span><br>\
					<span><button type="button" id="newThreadButton" name="newThread">Nové vlákno</button></span>'),
		templatePost: Handlebars.compile('<span>Autor: <input type="text" id="newPostAuthor" value="Janonymous"></span><br>\
					<span>Email: <input type="email" name="email" id="newThreadEmail" value="sage@sage.sage"></span><br>\
					<span>Predmet: <input type="text" name="subject" id="newPostSubject"></span><br>\
					<span>Komentár: <input type="text" name="content" id="newPostContent"></span><br>\
					<span>Súbor: <input type="file" name="img" id="newPostFile"></span><br>\
					<span>Adresa: <input type="text" name="link" id="newPostLink"></span><br>\
					<span><button type="button" id="newPostButton" name="newPost">Nový plagát</button></span>'),
		render: function(attr, elem) {
			let self = this;
			let content = this['template' + elem]();
			this.$el.html(content);
			$('#new' + elem + 'Button').click(function(ev) {
				console.log('Form.button');
				ev.preventDefault();
				let file = document.querySelector('#new' + elem + 'File').files[0];
				let model = elem === 'Thread' ? Threads : Posts;
				if (file)
				{
					let reader = new FileReader();
					reader.readAsDataURL(file);
					reader.addEventListener('load', function() {
						model.create({
							author:$('#new' + elem + 'Author').val(),
							email: $('#new' + elem + 'Email').val(),
							subject: $('#new' + elem + 'Subject').val(),
							content: $('#new' + elem + 'Content').val(),
							media: reader.result,
							media_orig: file.name,
						}, { wait: true });
					});
				} else {
					model.create({
						author:$('#new' + elem + 'Author').val(),
						email: $('#new' + elem + 'Email').val(),
						subject: $('#new' + elem + 'Subject').val(),
						content: $('#new' + elem + 'Content').val(),
					}, { wait: true });
				}
			});
		},
		hide: function() {
			this.$el.hide();
		},
		show: function() {
			this.$el.show();
		}
	});

	var PostModel = Backbone.Model.extend({
		idAttribute: '_id',
		initialize: function(attr, options) {
			let self = this;
			console.log('PostModel.initalize');
			this.view = new PostsView({
				model: this
			});
			this.on('add', function() { this.view.render(this.attributes); }, this);
			this.on('remove', function() {
				console.log('PostModel.remove');
			});
		}
	});

	var ThreadModel = Backbone.Model.extend({
		idAttribute: '_id',
		initialize: function(attr, options) {
			let self = this;
			console.log('ThreadModel.initialize');
			this.view = new ThreadsView({
				model: this
			});
			this.on('add', function() { this.view.render(this.attributes); }, this);
			this.on('remove', function() {
				console.log('ThreadMode.remove');
			});
		}
	});

	var App = new AppView();

	var Router = Backbone.Router.extend({
		routes: {
			"boards/:board/threads": 'getThreads',
			"boards/:board/threads/:thread/posts": 'getPosts',
			"": 'index'
		},
		getThreads: function(b, t) {
			console.log('getThreads');
			if (window.posts_refresh)
				clearInterval(posts_refresh);
			App.board = b;
			App.thread = undefined;
			App.trigger('change:thread');
			Threads.reset();
			Posts.reset();
			Threads.fetch();
			App.show();
			App.render(null, 'Thread');
		},
		getPosts: function(b, t) {
			console.log('getPosts');
			let self = this;
			App.board = b;
			App.thread = t;
			App.trigger('change:thread');
			Threads.reset();
			Posts.reset();
			Posts.fetch();
			App.show();
			App.render(null, 'Post');
			// if (window.posts_refresh)
			// 	clearInterval(posts_refresh);
			// window.posts_refresh = setInterval(() => { Posts.fetch(); }, 5000);
		},
		index: function() {
			App.hide();
			App.board = undefined;
			App.thread = undefined;
			$('#content').html('');
		}
	});

	var ThreadsCollection = Backbone.Collection.extend({
		model: ThreadModel,
		url: function() { return '/boards/' + App.board + '/threads'; }
	});

	var PostsCollection = Backbone.Collection.extend({
		model: PostModel,
		url: function() { return '/boards/' + App.board + '/threads/' + App.thread + '/posts'; }
	});

	Threads = new ThreadsCollection();
	Posts = new PostsCollection();

	console.log('start judechan.js');
	var router = new Router();
	Backbone.history.start();
});
