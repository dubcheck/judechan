const mongoose = require('mongoose');
const Schema = mongoose.Schema;
autoIncrement = require('mongoose-auto-increment');

const postSchema = mongoose.Schema({
	author: String,
	subject: String,
	content: String,
	media: String,
	media_orig: String,
	type: String,
	date: Date,
	board: String,
	thread: { type: Number, ref: 'Thread' }
});

autoIncrement.initialize(mongoose.connection);
const Post = mongoose.model('Post', postSchema);
postSchema.plugin(autoIncrement.plugin, { model: 'Post', startAt: 1 });
// Post.find().remove().exec();

// var In = mongoose.model("IdentityCounter");
// In.find({model: 'Post'}).then((res) => { console.log(res); });
// In.find().remove().exec();

module.exports = Post;