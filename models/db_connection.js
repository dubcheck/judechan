const mongoose = require('mongoose');
let connection = undefined;
mongoose.connect('mongodb://localhost/judechan');

module.exports = cb => {
	if(connection)
		cb(connection);
	else {
		connection = mongoose.connection;
		connection.on('error', console.error.bind(console, 'connection error:'));
		connection.once('open', () => {
  			console.log('Connected to mongodb');
  			cb(connection);
		});
	}
}
