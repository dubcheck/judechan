const mongoose = require('mongoose');
const Schema = mongoose.Schema;
autoIncrement = require('mongoose-auto-increment');

const threadSchema = mongoose.Schema({
  	author: String,
  	subject: String,
  	date: Date,
  	posts: [{ type: Number, ref: 'Post' }],
  	lastUpdate: Date,
  	board: String
});

autoIncrement.initialize(mongoose.connection);
const Thread = mongoose.model('Thread', threadSchema);
threadSchema.plugin(autoIncrement.plugin, { model: 'Thread', startAt: 1 });
// Thread.find().remove().exec();

// var In = mongoose.model("IdentityCounter");
// In.find({model: 'Post'}).then((res) => { console.log(res); });
// In.find().remove().exec();

module.exports = Thread;