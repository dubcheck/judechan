const express = require('express');
const app = express();
const router = express.Router();
const mongoose = require('mongoose');
const fs = require('fs');
const threads = require('./threads');

router.param('board', (req, res, next, board) => {
	res.resp_value = threads.filter((elem, arr, index) => {
		if(elem.board === board)
			return elem;
	});
	next();
});

router.route('/:board')
.get((req, res, next) => {
	res.json({});
})
.put((req, res, next) => {
	res.json({});
})
.post((req, res, next) => {
	res.json({});
})
.delete((req, res, next) => {
	res.json({});
})
.put((req, res, next) => {
	res.json({});
});

module.exports = router;