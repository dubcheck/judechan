const express = require('express');
const app = express();
const router = express.Router({ mergeParams: true });
const mongoose = require('mongoose');
const posts =  require('./posts');
const Post = require('../models/post-model');
const Thread = require('../models/thread-model');
const crypto = require('crypto');
const mkThumb = require('./mkThumb');
const exec = require('child_process').exec;
const fs = require('fs');

router.route('/')
.get((req, res, next) => {
	console.log(req.params);
	let board = req.params.board;
	let promise = Thread.find({ 'board': board }).sort({ lastUpdate: -1 }).populate('posts').limit(10).then((docs) => {
		res.json(docs);
	});
}).
put((req, res, next) => {
})
.post((req, res, next) => {
	console.log(req.params);
	fs.stat('./public/resources/', (err, stats) => {
		if (err) console.log(err);
		let saveThread = (media, media_orig, type) => {
			let date = bumpDate = new Date();
			let newThread = new Thread({
				author: req.body.author,
				subject: req.body.subject,
				content: req.body.content,
				date,
				lastUpdate: bumpDate,
				board: req.params.board
			});
			newThread.save((err, doc) => {
				if (err) throw err;
				let firstPost = new Post({
					author: req.body.author,
					email: req.body.email,
					subject: req.body.subject,
					content: req.body.content,
					media,
					media_orig,
					type,
					date,
					board: req.params.board,
					thread: doc._id
				});
				firstPost.save((err, post) => {
					if (err) throw err;
					doc.posts.push(post);
					doc.save((err, ndoc) => {
						const response = ndoc;
						response.posts = [ post ];
						res.json(response);
					});
				});
			});
		}
		if (!stats.isDirectory()) {
			fs.mkdirSync('.public/resources/');
		} else {
			if(!req.body.media) {
				res.render('error', {
	      			message: 'Prvy príspevok musí obsahovať mediálnu prílohu',
	      			err: 'Oprav to'
    			});
			} else {
				let regex = /data:(\w+)\/(\w+);base64,/
				let media = req.body.media.replace(regex, '');
				let type = req.app.locals.escapeShell(regex.exec(req.body.media)[1]);
				let ext = req.app.locals.escapeShell(regex.exec(req.body.media)[2]);

				const hash = crypto.createHash('sha');
				hash.update(media);
				let digest = hash.digest('hex');
				fs.writeFile(`public/resources/${digest}.${ext}`, media, { encoding: 'base64' }, (err) => {
					if (err) throw err;

					if (type === 'image') {
						if (ext.toLowerCase() === 'jpeg' || ext.toLowerCase() === 'jpg')
							exec(`jpegtran -copy none -outfile public/resources/${digest}.${ext} public/resources/${digest}.${ext}`, (err, res) => {
								if (err) throw err;
								mkThumb(type, digest, ext, req.app.locals.thumb_x, req.app.locals.thumb_y, (err, res) => {
									if (err) console.log(err);
									saveThread(`resources/${digest}_thumb.jpeg`, req.body.media_orig, type);
								});
							});
						else {
							mkThumb(type, digest, ext, req.app.locals.thumb_x, req.app.locals.thumb_y, (err, res) => {
								if (err) console.log(err);
								saveThread(`resources/${digest}_thumb.jpeg`, req.body.media_orig, type);
							});
						}
					} else if (type === 'video') {
						if (ext === 'mp4') {
							exec(`ffmpeg -i public/resources/${digest}${rand}.${ext}  -c copy -movflags +faststart -v error -y public/resources/${digest}${rand}.${ext}`, (err, res) => {
								if (err) console.log(err);
								mkThumb(type, digest, ext, req.app.locals.thumb_x, req.app.locals.thumb_y, (err, res) => {
									if (err) console.log(err);
									saveThread(`resources/${digest}_thumb.jpeg`, req.body.media_orig, type);
								});
							});
						} else {
							mkThumb(type, digest, ext, req.app.locals.thumb_x, req.app.locals.thumb_y, (err, res) => {
								if (err) console.log(err);
								saveThread(`resources/${digest}_thumb.jpeg`, req.body.media_orig, type);
							});
						}
					}
				});
			}
		}
	});
})
.delete((req, res, next) => {
	res.json({
		status: 'success'
	});
})
.put((req, res, next) => {
})
module.exports = router;
