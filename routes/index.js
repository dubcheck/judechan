const express = require('express');
const mongoose = require('mongoose');
const router = express.Router({ mergeParams: true });

const boardsSchema = mongoose.Schema({
	name: { type: String, index: true },
	description: String,
	date: Date,
	author: String,
	lastUpdate: Date,
});

const Board = mongoose.model('Board', boardsSchema);

router.get('/', (req, res, next) => {
	Board.find((err, boards) => {
		res.render('index', {
			title: 'Judechan',
			boards
		});
	});
});

module.exports = router;
