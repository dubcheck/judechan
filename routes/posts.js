const express = require('express');
const app = express();
const router = express.Router({ mergeParams: true });
const mongoose = require('mongoose');
const Post = require('../models/post-model');
const Thread = require('../models/thread-model');
const exec = require('child_process').exec;
const mkThumb = require('./mkThumb');

const crypto = require('crypto');
const fs = require('fs');
router.route('/')
.get((req, res, next) => {
	console.log(req.params);
	let board = req.params.board;
	let thread = req.params.thread;
	Post.find({ 'board': board, 'thread': thread }).sort({ _id: 1 }).then((docs) => {
		res.json(docs);
	});
})
.put((req, res, next) => {
	res.json({});
})
.post((req, res, next) => {
	function savePost(media, media_orig, type) {
		const date = new Date();
		Thread.update({ _id: req.params.thread, board: req.params.board }, { $set: { lastUpdate: date }}, (err, doc) => {
			if (err) throw err;
		});
		let post = new Post({
			author: req.body.author,
			email: req.body.email,
			subject: req.body.subject,
			content: req.body.content,
			media,
			media_orig,
			type,
			date,
			board: req.params.board,
			thread: req.params.thread
		});
		post.save((err, post) => {
			if (err) throw err;
			Thread.findOne({ board: req.params.board, _id: req.params.thread }).exec().then((thread) => {
				thread.posts.push(post);
				thread.save((err, tr) => { if (err) console.log(err); res.json(post); });
			});
		});
	}

	fs.stat('./public/resources/', (err, stats) => {
		if (err) {
			console.log(err);
			fs.mkdirSync('.public/resources/');
		}
		if (!stats.isDirectory())
			fs.mkdirSync('.public/resources/');
		if(!req.body.media) {
			savePost(null, null);
		} else {
			let regex = /data:(\w+)\/(\w+);base64,/
			let media = req.body.media.replace(regex, '');
			let ext = '';
			let type = '';
			let hash = crypto.createHash('sha');
			let rand = Math.random();

			type = req.app.locals.escapeShell(regex.exec(req.body.media)[1]);
			ext = req.app.locals.escapeShell(regex.exec(req.body.media)[2]);

			hash.update(media);
			let digest = hash.digest('hex');

			if (type === 'image') {
				fs.writeFile(`public/resources/${digest}${rand}.${ext}`, media, { encoding: 'base64' }, (err) => {
					if (err) throw err;
					if (ext.toLowerCase() === 'jpeg' || ext.toLowerCase() === 'jpg')
						exec(`jpegtran -copy none -outfile public/resources/${digest}.${ext} public/resources/${digest}${rand}.${ext}`, (err, res) => {
							if (err) console.log(err)
							mkThumb(type, digest, ext, req.app.locals.thumb_x, req.app.locals.thumb_y, (err, res) => {
								if (err) console.log(err);
								exec(`rm public/resources/${digest}${rand}.${ext}`, (err, res) => {
									if (err) console.log(err);
									savePost(`resources/${digest}_thumb.${ext}`, req.body.media_orig, type);
								});
							});
						});
					else {
						exec(`cp public/resources/${digest}${rand}.${ext} public/resources/${digest}.${ext}`, (err, res) => {
						 	if (err) console.log(err);
						 	mkThumb(type, digest, ext, req.app.locals.thumb_x, req.app.locals.thumb_y, (err, res) => {
								if (err) console.log(err);
								exec(`rm public/resources/${digest}${rand}.${ext}`, (err, res) => {
									if (err) console.log(err);
									savePost(`resources/${digest}_thumb.${ext}`, req.body.media_orig, type);
								});
							});
						});
					}
				});
			} else if (type === 'video') {
				let video_thumbnail = () => {
					exec(`cp public/resources/${digest}${rand}.${ext} public/resources/${digest}.${ext}`, (err, res) => {
						if (err) console.log(err);
						mkThumb(type, digest, ext, req.app.locals.thumb_x, req.app.locals.thumb_y, (err, res) => {
							if (err) console.log(err);
							exec(`rm public/resources/${digest}${rand}.${ext}`, (err, res) => {
								if (err) console.log(err);
								savePost(`resources/${digest}_thumb.jpeg`, req.body.media_orig, type);
							});
						});
					});
				}
				fs.writeFile(`public/resources/${digest}${rand}.${ext}`, media, { encoding: 'base64' }, (err) => {
					if (ext === 'mp4') {
						exec(`ffmpeg -i public/resources/${digest}${rand}.${ext}  -c copy -movflags +faststart -v error -y public/resources/${digest}${rand}.${ext}`, () => {
							if (err) console.log(err);
							video_thumbnail();
						});
					} else {
						video_thumbnail();
					}
				});
			} else {
				savePost();
			}

		}
	});
})
.delete((req, res, next) => {
	res.json({});
})
.put((req, res, next) => {
	res.json({});
});

module.exports = router;
