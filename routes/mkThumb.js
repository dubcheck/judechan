const exec = require('child_process').exec;

function mkThumb(type, name, ext, width, height, callback) {
	if (type === 'image') {
		exec(`gm convert public/resources/${name}.${ext} -quality 75 -thumbnail ${width}x${height} public/resources/${name}_thumb.${ext}`, callback);
	} else if (type === 'video') {
		console.log(type);
		exec(`ffmpeg -i public/resources/${name}.${ext} -v error -an -vframes 1 -f mjpeg -qscale:v 5 -filter_complex 'scale=iw*min(1\\,min(${width}/iw\\, ${height}/ih)):-1' -y public/resources/${name}_thumb.jpeg`, callback);
	}
}

module.exports = mkThumb;